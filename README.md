# AccountingApp

## PL

Accounting App jest to mikroserwis służący do przewalutowania z USD na PLN zapisywanych faktur.
Przewalutowanie odbywa się za pomocą łączenia z zewnętrznym api NBP. Kurs waluty pobierany jest po dacie księgowania, podanej podczas zapisania przedmiotu do bazy.
Aplikacja posiada odpowiednio możliwość filtrowania po nazwie wpisując tylko fragment nazwy oraz po dacie wpisując zakres dat. Jest możliwość sortowania po nazwie i dacie dodania. 

### Instrukcja uruchomienia aplikacji Accounting App 
1.  Sklonować repozytorium:

`git clone https://gitlab.com/tuchalskikamil/accountingapp.git`

2. Zbudować projekt, w zależności od systemu operacyjnego należy wykonać w głównym folderze projektu:

`./gradlew.bat clean build` (Windows) lub

`./gradlew clean build` 

3. Zainstalować i włączyć program XAMPP
4. Za pomocą programu XAMPP wystartować MySQL na porcie domyślnym 3306 oraz włączyć Apache 
5. Utworzyc pustą baze danych 'accountingapp' na `http://localhost/phpmyadmin/`
6. Przejść do folderu `~/accountingapp/build/lib`
7. Uruchomić plik jar używając Javy 17

`java -jar accountingApp-0.0.1-SNAPSHOT.jar`

8. Wykonać zapytania
- W programie Postman: 
     - zaimportować dane z pliku `~\accountingapp\postman\AccountingApp.postman_collection.json`
     - wysłać dane do bazy używając zapytania 'POST Add Computer'
     - pobrać dane z bazy używając zapytania 'Get All Computer'

- Bez programu Postman:
     - wysłać dane do zapisu pod adresem `http://localhost:9000/computer` metodą POST z header'em `Content-Type: application/json` i
body:
    `
    [
{
         "name": "komputer 1",
         "postingDate": "03-01-2022",
         "costUsd": 345
},
{
         "name": "komputer 2",
         "postingDate": "03-01-2022",
         "costUsd": 543
}
]
`
     - pobrać dane pod adresem `http://localhost:9000/computer`
- filtry i sortowanie są dostępne zmieniająć link: `http://localhost:9000/computer?name=kom&minPostingDate=2022-01-03&maxPostingDate=2022-01-04&sortedBy=postingDate&sortedBy=name`
9. Pliki XML zapisują się w folderze 'invoices' który tworzy się automatycznie w folderze, w którym znajduje się plik jar. '~/accountingapp/build/lib/invoices`

## ENG

Accounting App is a microservice for converting invoices from USD to PLN.
Currency conversion is done by connecting to external NBP api. The walty rate is charged using the posting date specified.
The application has the option of filtering by name by entering only a fragment of the name and by entering the date range by the date. It is possible to sort by name and date of posting.

### Instructions for starting Accounting App
1. Perform 

`git clone https://gitlab.com/tuchalskikamil/accountingapp.git`

2. Build project in main project's directory, depending on operating system

`./gradlew.bat clean build` (Windows) or

`./gradlew clean build `

3. Install and start the XAMPP program:
4. Using XAMPP, start MySQL on the default port 3306 and turn on Apache
5. Create empty 'accountingapp' database on `http://localhost/phpmyadmin/`
6. Go to `~/accountingapp/build/lib`
7. Run the jar file using Java 17

`java -jar accountingApp-0.0.1-SNAPSHOT.jar`

8. Run requests
- in the Postman program:
    - Import data from '~\accountingapp\postman\AccountingApp.postman_collection.json'
    - send data using 'POST Add Computer'
    - download data using 'Get All Computer'
- Without Postman:
    - send data at the address `http://localhost:9000/computer` using POST method with header `Content-Type: application/json` and
body: `[
{
        "name": "komputer 1",
        "postingDate": "03-01-2022",
        "costUsd": 345
},
{
        "name": "komputer 2",
        "postingDate": "03-01-2022",
        "costUsd": 543
}
]`
    - download the data at `http://localhost:9000/computer`
- filters and sorting are available by changing link: `http://localhost:9000/computer?name=kom&minPostingDate=2022-01-03&maxPostingDate=2022-01-04&sortedBy=postingDate&sortedBy=name`

9. XML files are saved in the 'invoices' directory which is created automatically in the folder where the jar. file is located '~/accountingapp/build/lib/invoices'
