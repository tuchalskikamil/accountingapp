package pl.tuchalski.kamil.accountingApp.exchangeRateNbp;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Class representing rate
 */
public class Rate {
    private String no;
    private LocalDate effectiveDate;
    private BigDecimal mid;

    /**
     * Instantiates rate
     */
    public Rate() {

    }

    /**
     * Instantiates rate
     *
     * @param no            parameter taken from external api
     * @param effectiveDate parameter taken from external api
     * @param mid           exchange rate parameter taken from external api
     */
    public Rate(final String no, final LocalDate effectiveDate, final BigDecimal mid) {
        this.no = no;
        this.effectiveDate = effectiveDate;
        this.mid = mid;
    }

    public String getNo() {
        return no;
    }

    public void setNo(final String no) {
        this.no = no;
    }

    public LocalDate getEffectiveDate() {
        return effectiveDate;
    }

    public void setEffectiveDate(final LocalDate effectiveDate) {
        this.effectiveDate = effectiveDate;
    }

    public BigDecimal getMid() {
        return mid;
    }

    public void setMid(final BigDecimal mid) {
        this.mid = mid;
    }

    @Override
    public String toString() {
        return "Rate{" +
                "no='" + no + '\'' +
                ", effectiveDate=" + effectiveDate +
                ", mid=" + mid +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Rate rate = (Rate) o;
        {
            return Objects.equals(no, rate.no)
                    && Objects.equals(effectiveDate, rate.effectiveDate)
                    && Objects.equals(mid, rate.mid);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(no, effectiveDate, mid);
    }
}
