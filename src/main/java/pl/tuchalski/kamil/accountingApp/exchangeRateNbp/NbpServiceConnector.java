package pl.tuchalski.kamil.accountingApp.exchangeRateNbp;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.time.LocalDate;

/**
 * Class representing the connection to the NBP server
 */
@Component
public class NbpServiceConnector {
    private final WebClient webClient;

    /**
     * Instantiates NBP service connector
     *
     * @param webClient for connection with NBP service
     */
    public NbpServiceConnector(final WebClient webClient) {
        this.webClient = webClient;
    }

    /**
     * Connects to NBP service
     *
     * @param date currency conversion rates date
     * @return exchange rate
     */
    public ExchangeUsdToPln fetchExchangeUsdToPln(final LocalDate date) {
        return webClient
                .get()
                .uri(uriBuilder -> uriBuilder
                        .path("/exchangerates/rates/a/usd/{date}/")
                        .queryParam("format", "json")
                        .build(date))
                .retrieve()
                .toEntity(ExchangeUsdToPln.class)
                .block()
                .getBody();
    }
}
