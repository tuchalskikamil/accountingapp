package pl.tuchalski.kamil.accountingApp.exchangeRateNbp;

import java.util.List;
import java.util.Objects;

/**
 * Class representing exchange rate
 */
public class ExchangeUsdToPln {
    private String table;
    private String currency;
    private String code;
    private List<Rate> rates;

    /**
     * Instantiates exchange rate
     */
    public ExchangeUsdToPln() {

    }

    /**
     * Instantiates exchange rate
     *
     * @param table    parameter taken from external api
     * @param currency parameter taken from external api
     * @param code     parameter taken from external api
     * @param rates    parameter taken from external api
     */
    public ExchangeUsdToPln(final String table, final String currency, final String code, final List<Rate> rates) {
        this.table = table;
        this.currency = currency;
        this.code = code;
        this.rates = rates;
    }

    public String getTable() {
        return table;
    }

    public void setTable(final String table) {
        this.table = table;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(final String currency) {
        this.currency = currency;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public List<Rate> getRates() {
        return rates;
    }

    public void setRates(final List<Rate> rates) {
        this.rates = rates;
    }

    @Override
    public String toString() {
        return "ExchangeUsdToPln{" +
                "table='" + table + '\'' +
                ", currency='" + currency + '\'' +
                ", code='" + code + '\'' +
                ", rates=" + rates +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ExchangeUsdToPln that = (ExchangeUsdToPln) o;
        {
            return Objects.equals(table, that.table)
                    && Objects.equals(currency, that.currency)
                    && Objects.equals(code, that.code)
                    && Objects.equals(rates, that.rates);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(table, currency, code, rates);
    }
}