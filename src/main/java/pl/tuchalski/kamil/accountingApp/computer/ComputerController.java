package pl.tuchalski.kamil.accountingApp.computer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

/**
 * Rest controller to manage computers
 */
@RestController
@Validated
public class ComputerController {
    private static final Logger logger = LoggerFactory.getLogger(ComputerController.class);

    private final ComputerService computerService;

    /**
     * Instantiates ComputerController
     *
     * @param computerService computer service
     */
    public ComputerController(final ComputerService computerService) {
        this.computerService = computerService;
    }

    /**
     * Endpoint adding new computer
     *
     * @param computerDtos new computers
     * @return Status 200 - OK
     */
    @PostMapping("/computer")
    public List<ComputerDto> addComputer(@NotEmpty @RequestBody final List<@Valid ComputerSaveDto> computerDtos) {
        logger.info("POST /computer request received with body: " + computerDtos);
        final List<ComputerDto> addedComputers = computerService.addComputer(computerDtos);
        logger.info("Returning " + addedComputers);
        return addedComputers;
    }

    /**
     * Endpoint returning computers filtered by name and date
     *
     * @param name           parameter filter
     * @param minPostingDate parameter filter
     * @param maxPostingDate parameter filter
     * @param sortedBy       to sort by given values
     * @return computers filtered by name and date
     */
    @GetMapping(value = "/computer")
    public List<ComputerDto> getAllComputer(@RequestParam(defaultValue = "") final String name,
                                            @RequestParam(defaultValue = "#{T(java.time.LocalDate).EPOCH}")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate minPostingDate,
                                            @RequestParam(defaultValue = "#{T(java.time.LocalDate).now()}")
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) final LocalDate maxPostingDate,
                                            @RequestParam(defaultValue = "") final List<@Pattern(regexp = "name" +
                                                    "|postingDate") String> sortedBy
    ) {
        logger.info("GET/computers name=" + name + " minPostingDate=" + minPostingDate +
                " maxPostingDate=" + maxPostingDate + " sortedBy=" + sortedBy +
                " get computers request received");
        final List<ComputerDto> allComputers = computerService.getAllComputers(
                name,
                minPostingDate,
                maxPostingDate,
                sortedBy.toArray(String[]::new)
        );
        logger.info("Returning computers: " + allComputers);
        return allComputers;
    }
}
