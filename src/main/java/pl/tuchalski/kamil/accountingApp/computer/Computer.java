package pl.tuchalski.kamil.accountingApp.computer;

import pl.tuchalski.kamil.accountingApp.invoice.LocalDateAdapter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Class representing computer
 */
@XmlType(propOrder = {"name", "postingDate", "costUsd", "costPln"})
@XmlRootElement(name = "komputer")
@Entity
public class Computer {
    private String name;
    private LocalDate postingDate;
    private BigDecimal costUsd;
    private BigDecimal costPln;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /**
     * Instantiates Computer
     */
    public Computer() {

    }

    /**
     * Instantiates Computer
     *
     * @param name        name of the computer
     * @param postingDate posting date of the computer
     * @param costUsd     cost in USD
     * @param costPln     cost in PLN
     */
    public Computer(final String name,
                    final LocalDate postingDate,
                    final BigDecimal costUsd,
                    final BigDecimal costPln) {
        this.name = name;
        this.postingDate = postingDate;
        this.costUsd = costUsd;
        this.costPln = costPln;
    }

    public BigDecimal getCostPln() {
        return costPln;
    }

    @XmlElement(name = "koszt_PLN")
    public void setCostPln(final BigDecimal costPln) {
        this.costPln = costPln;
    }

    public String getName() {
        return name;
    }

    @XmlElement(name = "nazwa")
    public void setName(final String name) {
        this.name = name;
    }

    public LocalDate getPostingDate() {
        return postingDate;
    }

    @XmlJavaTypeAdapter(value = LocalDateAdapter.class)
    @XmlElement(name = "data_ksiegowania")
    public void setPostingDate(final LocalDate postingDate) {
        this.postingDate = postingDate;
    }

    public BigDecimal getCostUsd() {
        return costUsd;
    }

    @XmlElement(name = "koszt_USD")
    public void setCostUsd(final BigDecimal costUsd) {
        this.costUsd = costUsd;
    }

    public Long getId() {
        return id;
    }

    @XmlTransient
    public void setId(final Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "Computer{" +
                "name='" + name + '\'' +
                ", postingDate=" + postingDate +
                ", costUsd=" + costUsd +
                ", costPln=" + costPln +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Computer computer = (Computer) o;
        {
            return Objects.equals(name, computer.name)
                    && Objects.equals(postingDate, computer.postingDate)
                    && Objects.equals(costUsd, computer.costUsd)
                    && Objects.equals(costPln, computer.costPln)
                    && Objects.equals(id, computer.id);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, postingDate, costUsd, costPln, id);
    }
}
