package pl.tuchalski.kamil.accountingApp.computer;

import org.springframework.stereotype.Component;
import pl.tuchalski.kamil.accountingApp.exchangeRateNbp.ExchangeUsdToPln;
import pl.tuchalski.kamil.accountingApp.exchangeRateNbp.NbpServiceConnector;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;

/**
 * Class to convert computer
 */
@Component
public class ComputerConverter {

    private final NbpServiceConnector nbpServiceConnector;

    /**
     * Instantiates Computer converter
     *
     * @param nbpServiceConnector connection to NBP service
     */
    public ComputerConverter(final NbpServiceConnector nbpServiceConnector) {
        this.nbpServiceConnector = nbpServiceConnector;
    }

    /**
     * Method converting ComputerSaveDto to ComputerDto
     *
     * @param computerSaveDto input dto
     * @return ComputerDto
     */
    public ComputerDto convertToComputerDto(final ComputerSaveDto computerSaveDto) {
                return new ComputerDto(
                computerSaveDto.getName(),
                computerSaveDto.getPostingDate(),
                computerSaveDto.getCostUsd().setScale(2, RoundingMode.HALF_UP),
                convertUsdToPln(computerSaveDto.getPostingDate(), computerSaveDto.getCostUsd()).setScale(2,
                        RoundingMode.HALF_UP));
    }

    /**
     * Method converting ComputerDTO to Computer
     *
     * @param computerDto dto
     * @return Computer
     */
    public Computer convertToComputer(final ComputerDto computerDto) {
        return new Computer(
                computerDto.getName(),
                computerDto.getPostingDate(),
                computerDto.getCostUsd(),
                computerDto.getCostPln()
        );
    }

    /**
     * Method converting Computer to ComputerDto
     *
     * @param computer computer
     * @return ComputerDto
     */
    public ComputerDto convertToComputerDtoFromComputer(final Computer computer) {
        return new ComputerDto(
                computer.getName(),
                computer.getPostingDate(),
                computer.getCostUsd(),
                computer.getCostPln()
        );
    }



    private BigDecimal convertUsdToPln(final LocalDate date, final BigDecimal cosUsd) {
        final ExchangeUsdToPln exchangeUsdToPln = nbpServiceConnector.fetchExchangeUsdToPln(date);
        final BigDecimal midRate = exchangeUsdToPln.getRates().get(0).getMid();
        return midRate.multiply(cosUsd);
    }
}
