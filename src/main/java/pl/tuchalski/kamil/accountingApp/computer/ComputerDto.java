package pl.tuchalski.kamil.accountingApp.computer;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Class representing computer DTO
 */
@JsonInclude
public class ComputerDto extends ComputerSaveDto {

    private BigDecimal costPln;

    /**
     * Instantiates Computer DTO
     */
    public ComputerDto() {

    }

    /**
     * Instantiates Computer DTO
     *
     * @param name        name of the computer
     * @param postingDate computer shopping date
     * @param costUsd     computer cost in USD
     * @param costPln     computer cost in PLN
     */
    public ComputerDto(final String name,
                       final LocalDate postingDate,
                       final BigDecimal costUsd,
                       final BigDecimal costPln) {
        super(name, postingDate, costUsd);
        this.costPln = costPln;
    }

    public BigDecimal getCostPln() {
        return costPln;
    }

    public void setCostPln(final BigDecimal costPln) {
        this.costPln = costPln;
    }

    @Override
    public String toString() {
        return "ComputerDto{" +
                "costPln=" + costPln +
                "} " + super.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        if (!super.equals(o)) {
            return false;
        }
        ComputerDto that = (ComputerDto) o;
        {
            return Objects.equals(costPln, that.costPln);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), costPln);
    }
}


