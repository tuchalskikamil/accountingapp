package pl.tuchalski.kamil.accountingApp.computer;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Class representing computer save DTO
 */
public class ComputerSaveDto {
    @NotBlank(message = "Name is mandatory and should not be empty ")
    private String name;
    @JsonFormat(pattern = "dd-MM-yyyy")
    @NotNull(message = "Posting date is mandatory and should not be empty")
    private LocalDate postingDate;
    @NotNull(message = "Cost is mandatory and should be greater or equal to 0")
    @Min(0)
    private BigDecimal costUsd;

    /**
     * Instantiates Computer
     */
    public ComputerSaveDto() {

    }

    /**
     * Instantiates Computer
     *
     * @param name        name of the computer
     * @param postingDate computer shopping date
     * @param costUsd     computer cost in USD
     */
    public ComputerSaveDto(final String name, final LocalDate postingDate, final BigDecimal costUsd) {
        this.name = name;
        this.postingDate = postingDate;
        this.costUsd = costUsd;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public LocalDate getPostingDate() {
        return postingDate;
    }

    public void setPostingDate(final LocalDate postingDate) {
        this.postingDate = postingDate;
    }

    public BigDecimal getCostUsd() {
        return costUsd;
    }

    public void setCostUsd(final BigDecimal costUsd) {
        this.costUsd = costUsd;
    }

    @Override
    public String toString() {
        return "ComputerSaveDto{" +
                "name='" + name + '\'' +
                ", postingDate=" + postingDate +
                ", costUsd=" + costUsd +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ComputerSaveDto that = (ComputerSaveDto) o;
        {
            return Objects.equals(name, that.name)
                    && Objects.equals(postingDate, that.postingDate)
                    && Objects.equals(costUsd, that.costUsd);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, postingDate, costUsd);
    }
}
