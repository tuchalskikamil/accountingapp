package pl.tuchalski.kamil.accountingApp.computer;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import pl.tuchalski.kamil.accountingApp.invoice.XmlConverter;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Service for computers
 */
@Service
public class ComputerService {
    private final ComputerRepository computerRepository;
    private final ComputerConverter computerConverter;
    private final XmlConverter xmlConverter;

    /**
     * Instantiates ComputerService
     *
     * @param computerRepository for database connection
     * @param computerConverter  convert ComputerSaveDto to ComputerDto and to Computer
     * @param xmlConverter       save XML to file
     */
    public ComputerService(final ComputerRepository computerRepository,
                           final ComputerConverter computerConverter,
                           final XmlConverter xmlConverter) {
        this.computerRepository = computerRepository;
        this.computerConverter = computerConverter;
        this.xmlConverter = xmlConverter;
    }

    /**
     * Method adding new computer
     *
     * @param computerSaveDtos saved new computers to database
     */
    public List<ComputerDto> addComputer(final List<ComputerSaveDto> computerSaveDtos) {
        final List<ComputerDto> computerDtos = convertComputerDtosFrom(computerSaveDtos);
        final List<Computer> computers = saveComputers(computerDtos);
        xmlConverter.saveToFile(computers);
        return computerDtos;
    }

    /**
     * Returning computers filtered by name and date
     *
     * @param name           parameter filter
     * @param minPostingDate parameter filter
     * @param maxPostingDate parameter filter
     * @param sortedBy       to sort results
     * @return computers filtered by name and date
     */
    public List<ComputerDto> getAllComputers(final String name,
                                             final LocalDate minPostingDate,
                                             final LocalDate maxPostingDate,
                                             final String[] sortedBy) {
        return convertComputerFrom(computerRepository.findByNameContainingAndPostingDateBetween(name,
                minPostingDate,
                maxPostingDate,
                Sort.by(sortedBy)));
    }

    private List<Computer> saveComputers(List<ComputerDto> computerDtos) {
        return computerDtos.stream()
                .map(computerConverter::convertToComputer)
                .peek(computerRepository::save)
                .collect(Collectors.toList());
    }

    private List<ComputerDto> convertComputerDtosFrom(final List<ComputerSaveDto> computerSaveDtos) {
        return computerSaveDtos
                .stream()
                .map(computerConverter::convertToComputerDto)
                .collect(Collectors.toList());
    }

    private List<ComputerDto> convertComputerFrom(final List<Computer> computers) {
        return computers
                .stream()
                .map(computerConverter::convertToComputerDtoFromComputer)
                .collect(Collectors.toList());
    }

}
