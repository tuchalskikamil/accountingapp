package pl.tuchalski.kamil.accountingApp.computer;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

/**
 * Computer database management
 */
@Repository
public interface ComputerRepository extends JpaRepository<Computer, Long> {

    List<Computer> findByNameContainingAndPostingDateBetween(@Param("name") final String name,
                                                             @Param("minPostingDate") final LocalDate minPostingDate,
                                                             @Param("maxPostingDate") final LocalDate maxPostingDate,
                                                             final Sort sort);

}
