package pl.tuchalski.kamil.accountingApp.errors;

import java.util.Objects;

/**
 * Class representing error message
 */
public class ErrorMessage {
    private final String errorName;

    /**
     * Instantiates ErrorMessage
     *
     * @param errorName Description of error
     */
    public ErrorMessage(final String errorName) {
        this.errorName = errorName;
    }

    public String getErrorName() {
        return errorName;
    }

    @Override
    public String toString() {
        return "ErrorMessage{" +
                "errorName='" + errorName + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ErrorMessage that = (ErrorMessage) o;
        {
            return Objects.equals(errorName, that.errorName);
        }
    }

    @Override
    public int hashCode() {
        return Objects.hash(errorName);
    }
}
