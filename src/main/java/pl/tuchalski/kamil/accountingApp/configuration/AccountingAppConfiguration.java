package pl.tuchalski.kamil.accountingApp.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

/**
 * Configuration class
 */
@Configuration
public class AccountingAppConfiguration {

    /**
     * Instantiates webClient
     *
     * @return created bean webClient
     */
    @Bean
    public WebClient webClient(@Value("${nbp.api.url}")final String nbpApiUrl) {
        return WebClient.create(nbpApiUrl);
    }
}
