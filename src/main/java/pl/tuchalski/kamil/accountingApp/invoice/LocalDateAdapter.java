package pl.tuchalski.kamil.accountingApp.invoice;

import org.springframework.stereotype.Component;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

/**
 * XML adapter for LocalDate
 */
@Component
public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {
    /**
     * Converts String to LocalDate
     *
     * @param value String date
     * @return LocalDate date
     */
    public LocalDate unmarshal(final String value) {
        return LocalDate.parse(value);
    }

    /**
     * Converts LocalDate to String
     *
     * @param value LocalDate date
     * @return String date
     */
    public String marshal(final LocalDate value) {
        return value.toString();
    }
}
