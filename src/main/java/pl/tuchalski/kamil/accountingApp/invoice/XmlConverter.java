package pl.tuchalski.kamil.accountingApp.invoice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import pl.tuchalski.kamil.accountingApp.computer.Computer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.List;

/**
 * For converting objects to XML file
 */
@Component
public class XmlConverter {

    private static final Logger logger = LoggerFactory.getLogger(XmlConverter.class);
    private final String filePath;

    public XmlConverter(@Value("${invoices.directory}") final String filePath) {
        this.filePath = filePath;
    }

    /**
     * Save XML to file
     *
     * @param computers List of the computers to save
     */
    public void saveToFile(final List<Computer> computers) {
        try {
            final JAXBContext context = JAXBContext.newInstance(Invoice.class);
            final Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            createDirectory();
            final File file = new File(filePath + "faktura_" + Instant.now().getEpochSecond() + ".xml");
            marshaller.marshal(new Invoice(computers), file);
        } catch (JAXBException | IOException ex) {
            logger.error("Cannot save invoice to XML", ex);
        }
    }

    private void createDirectory() throws IOException {
        final Path path = Paths.get(filePath);
        Files.createDirectories(path);
    }
}
