package pl.tuchalski.kamil.accountingApp.invoice;

import pl.tuchalski.kamil.accountingApp.computer.Computer;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;
import java.util.Objects;

/**
 * Invoice XML root object
 */
@XmlRootElement(name = "faktura")
public class Invoice {
    private List<Computer> computers;

    /**
     * Instantiates Invoice
     */
    public Invoice() {

    }

    /**
     * Instantiates Invoice
     *
     * @param computers List of the computers
     */
    public Invoice(final List<Computer> computers) {
        this.computers = computers;
    }

    public List<Computer> getComputers() {
        return computers;
    }

    @XmlElement(name = "komputer")
    public void setComputers(final List<Computer> computers) {
        this.computers = computers;
    }

    @Override
    public String toString() {
        return "Invoice{" +
                "computers=" + computers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Invoice invoice = (Invoice) o;
        return Objects.equals(computers, invoice.computers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(computers);
    }
}
