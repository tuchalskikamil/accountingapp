package pl.tuchalski.kamil.accountingApp.errors;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class ErrorHandlerTest {
    @Mock
    private MethodArgumentNotValidException exception;

    @Test
    @DisplayName("Should return validation error response")
    void shouldReturnValidationErrorResponse() {
        //given
        final ErrorHandler errorHandler = new ErrorHandler();
        //when
        final ResponseEntity<ErrorMessage> result = errorHandler.validationErrorHandler(exception);
        //then
        assertThat(result.getBody()).isEqualTo(new ErrorMessage("Validation Error"));

    }

    @Test
    @DisplayName("Should return internal server error")
    void shouldReturnInternalServerError() {
        //given
        final ErrorHandler errorHandler = new ErrorHandler();
        //when
        final ResponseEntity<ErrorMessage> result = errorHandler.serverErrorHandler(exception);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Test
    @DisplayName("Should return server error response")
    void shouldReturnServerErrorResponse() {
        //given
        final ErrorHandler errorHandler = new ErrorHandler();
        //when
        final ResponseEntity<ErrorMessage> result = errorHandler.serverErrorHandler(exception);
        //then
        assertThat(result.getBody()).isEqualTo(new ErrorMessage("Server Error"));
    }

    @Test
    @DisplayName("Should return status Bad Request")
    void shouldReturnStatusBadRequest() {
        //given
        final ErrorHandler errorHandler = new ErrorHandler();
        //when
        final ResponseEntity<ErrorMessage> result = errorHandler.validationErrorHandler(exception);
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
    }

}