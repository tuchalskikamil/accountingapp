package pl.tuchalski.kamil.accountingApp.invoice;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;

class LocalDateAdapterTest {

    private final LocalDateAdapter localDateAdapter = new LocalDateAdapter();

    @Test
    @DisplayName("Should return LocalDate")
    void shouldReturnLocalDate() {
        //given
        final String date = "2022-04-22";
        final LocalDate date1 = LocalDate.parse("2022-04-22");
        //when
        final LocalDate result = localDateAdapter.unmarshal(date);
        //then
        assertThat(result).isEqualTo(date1);
    }

    @Test
    @DisplayName("Should return String")
    void shouldReturnString() {
        //given
        final String date = "2022-04-22";
        final LocalDate date1 = LocalDate.parse("2022-04-22");
        //when
        final String result = localDateAdapter.marshal(date1);
        //then
        assertThat(result).isEqualTo(date);
    }

}