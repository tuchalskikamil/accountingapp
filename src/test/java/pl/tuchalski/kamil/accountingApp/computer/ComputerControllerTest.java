package pl.tuchalski.kamil.accountingApp.computer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ComputerControllerTest {

    private ComputerController computerController;
    @Mock
    private ComputerService computerService;

    @BeforeEach
    void setup() {
        computerController = new ComputerController(computerService);
    }

    @Test
    @DisplayName("Should return computers for save")
    void shouldReturnComputersForSave() {
        //given
        final List<ComputerDto> computerDtos = List.of(
                new ComputerDto("komputer 1", LocalDate.parse("2020-02-05"), new BigDecimal("356.56"),
                        new BigDecimal("1426.24")),
                new ComputerDto("komputer 2", LocalDate.parse("2020-02-05"), new BigDecimal("356.56"),
                        new BigDecimal("1426.24"))
        );
        final List<ComputerSaveDto> computerSaveDtos = List.of(
                new ComputerSaveDto("komputer 1", LocalDate.parse("2020-02-05"), new BigDecimal("356.56")),
                new ComputerSaveDto("komputer 2", LocalDate.parse("2020-02-05"), new BigDecimal("356.56"))
        );
        when(computerService.addComputer(computerSaveDtos)).thenReturn(computerDtos);
        //when
        final List<ComputerDto> result = computerController.addComputer(computerSaveDtos);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(computerDtos);
    }

    @Test
    @DisplayName("Should return computers")
    void shouldReturnComputers() {
        //given
        final String name = "komp";
        final LocalDate minDate = LocalDate.parse("2022-01-02");
        final LocalDate maxDate = LocalDate.parse("2022-02-02");
        final List<String> sortedBy = List.of("name");
        final List<ComputerDto> computerDtos = List.of(
                new ComputerDto("komputer 1", LocalDate.parse("2020-02-05"), new BigDecimal("356.56"),
                        new BigDecimal("1426.24")),
                new ComputerDto("komputer 2", LocalDate.parse("2020-02-05"), new BigDecimal("356.56"),
                        new BigDecimal("1426.24"))
        );
        when(computerService.getAllComputers(name, minDate, maxDate, sortedBy.toArray(String[]::new)))
                .thenReturn(computerDtos);
        //when
        final List<ComputerDto> result = computerController.getAllComputer(name, minDate, maxDate, sortedBy);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(computerDtos);

    }

}