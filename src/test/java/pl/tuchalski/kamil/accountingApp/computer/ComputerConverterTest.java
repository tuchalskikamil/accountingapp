package pl.tuchalski.kamil.accountingApp.computer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.tuchalski.kamil.accountingApp.exchangeRateNbp.ExchangeUsdToPln;
import pl.tuchalski.kamil.accountingApp.exchangeRateNbp.NbpServiceConnector;
import pl.tuchalski.kamil.accountingApp.exchangeRateNbp.Rate;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ComputerConverterTest {

    @Mock
    private NbpServiceConnector nbpServiceConnector;
    private ComputerConverter computerConverter;

    @BeforeEach
    void setup() {
        computerConverter = new ComputerConverter(nbpServiceConnector);
    }

    @Test
    @DisplayName("Should return  ComputerDto")
    void shouldReturnComputerDto() {
        //given
        final ComputerSaveDto computerSaveDto1 = createComputerSaveDto("komputer1");
        final ComputerDto computerDto1 = createComputerDto("komputer1");
        when(nbpServiceConnector.fetchExchangeUsdToPln(LocalDate.parse("2020-02-05"))).thenReturn(createExchange());
        //when
        final ComputerDto result = computerConverter.convertToComputerDto(computerSaveDto1);
        //then
        assertThat(result).isEqualTo(computerDto1);
    }

    @Test
    @DisplayName("Should return Computer")
    void shouldReturnComputer() {
        //given
        final ComputerDto computerDto = createComputerDto("komputer1");
        final Computer computer = createComputer("komputer1");
        //when
        final Computer result = computerConverter.convertToComputer(computerDto);
        //then
        assertThat(result).isEqualTo(computer);
    }

    @Test
    @DisplayName("Should return ComputerDto converted from Computer")
    void shouldReturnComputerDtoFromComputer() {
        //given
        final ComputerDto computerDto = createComputerDto("komputer1");
        final Computer computer = createComputer("komputer1");
        //when
        final ComputerDto result = computerConverter.convertToComputerDtoFromComputer(computer);
        //then
        assertThat(result).isEqualTo(computerDto);
    }

    private ExchangeUsdToPln createExchange() {
        return new ExchangeUsdToPln("A", "dolar amerykański", "USD",
                List.of(new Rate("005/A/NBP/2022", LocalDate.parse("2022-01-10"), new BigDecimal("4.0064"))));

    }

    private ComputerSaveDto createComputerSaveDto(final String name) {
        return new ComputerSaveDto(name, LocalDate.parse("2020-02-05"), new BigDecimal("356.56"));
    }

    private ComputerDto createComputerDto(final String name) {
        return new ComputerDto(name,
                LocalDate.parse("2020-02-05"),
                new BigDecimal("356.56"),
                new BigDecimal("1428.52"));
    }

    private Computer createComputer(final String name) {
        return new Computer(name,
                LocalDate.parse("2020-02-05"),
                new BigDecimal("356.56"),
                new BigDecimal("1428.52"));
    }
}