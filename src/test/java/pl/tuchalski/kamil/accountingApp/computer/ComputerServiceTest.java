package pl.tuchalski.kamil.accountingApp.computer;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Sort;
import pl.tuchalski.kamil.accountingApp.invoice.XmlConverter;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ComputerServiceTest {

    private ComputerService computerService;
    @Mock
    private ComputerRepository computerRepository;
    @Mock
    private ComputerConverter computerConverter;
    @Mock
    private XmlConverter xmlConverter;

    @BeforeEach
    void setup() {
        computerService = new ComputerService(computerRepository, computerConverter, xmlConverter);
    }

    @Test
    @DisplayName("Should verify save")
    void shouldVerifySave() {
        //given
        final ComputerSaveDto computerSaveDto1 = createComputerSaveDto("komputer1");
        final ComputerSaveDto computerSaveDto2 = createComputerSaveDto("komputer2");
        final ComputerDto computerDto1 = createComputerDto("komputer1");
        final ComputerDto computerDto2 = createComputerDto("komputer2");
        final Computer computer1 = createComputer("komputer1");
        final Computer computer2 = createComputer("komputer2");
        when(computerConverter.convertToComputer(computerDto1)).thenReturn(computer1);
        when(computerConverter.convertToComputer(computerDto2)).thenReturn(computer2);
        when(computerConverter.convertToComputerDto(computerSaveDto1)).thenReturn(computerDto1);
        when(computerConverter.convertToComputerDto(computerSaveDto2)).thenReturn(computerDto2);
        //when
        final List<ComputerDto> result = computerService.addComputer(List.of(computerSaveDto1, computerSaveDto2));
        //then
        verify(computerRepository).save(computer1);
        verify(computerRepository).save(computer2);
        verify(xmlConverter).saveToFile(List.of(computer1, computer2));
        assertThat(result).containsExactlyInAnyOrderElementsOf(List.of(computerDto1, computerDto2));
    }

    @Test
    @DisplayName("Should return computers")
    void shouldReturnComputer() {
        //given
        final String name = "kom";
        final LocalDate minDate = LocalDate.parse("2022-01-02");
        final LocalDate maxDate = LocalDate.parse("2022-02-02");
        final String[] sortedBy = {"name"};
        final ComputerDto computerDto1 = createComputerDto("komputer1");
        final ComputerDto computerDto2 = createComputerDto("komputer2");
        final Computer computer1 = createComputer("komputer1");
        final Computer computer2 = createComputer("komputer2");
        when(computerRepository.findByNameContainingAndPostingDateBetween(name, minDate, maxDate, Sort.by(sortedBy)))
                .thenReturn(List.of(computer1, computer2));
        when(computerConverter.convertToComputerDtoFromComputer(computer1)).thenReturn(computerDto1);
        when(computerConverter.convertToComputerDtoFromComputer(computer2)).thenReturn(computerDto2);
        //when
        final List<ComputerDto> result = computerService.getAllComputers(name, minDate, maxDate, sortedBy);
        //then
        assertThat(result).containsExactlyInAnyOrderElementsOf(List.of(computerDto1, computerDto2));
    }

    private ComputerSaveDto createComputerSaveDto(final String name) {
        return new ComputerSaveDto(name, LocalDate.parse("2020-02-05"), new BigDecimal("356.56"));
    }

    private ComputerDto createComputerDto(final String name) {
        return new ComputerDto(name,
                LocalDate.parse("2020-02-05"),
                new BigDecimal("356.56"),
                new BigDecimal("1426.24"));
    }

    private Computer createComputer(final String name) {
        return new Computer(name,
                LocalDate.parse("2020-02-05"),
                new BigDecimal("356.56"),
                new BigDecimal("1426.24"));
    }

}