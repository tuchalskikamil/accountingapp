package pl.tuchalski.kamil.accountingApp.computer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

class ComputerDtoTest {
    private ValidatorFactory validatorFactory;
    private Validator validator;

    public static Stream<Arguments> getComputers() {
        return Stream.of(
                Arguments.of(new ComputerDto("komputer 1", LocalDate.parse("2020-02-05"),
                        new BigDecimal("356.56"),new BigDecimal("1426.24")), 0),
                Arguments.of(new ComputerDto("", LocalDate.parse("2020-02-05"),
                        new BigDecimal("356.56"),new BigDecimal("1426.24")), 1),
                Arguments.of(new ComputerDto(null, LocalDate.parse("2020-02-05"),
                        new BigDecimal("356.56"),new BigDecimal("1426.24")), 1),
                Arguments.of(new ComputerDto("komputer 1", null,
                        new BigDecimal("356.56"),new BigDecimal("1426.24")), 1),
                Arguments.of(new ComputerDto("komputer 1", LocalDate.parse("2020-02-05"),
                        new BigDecimal("-356.56"),new BigDecimal("1426.24")), 1),
                Arguments.of(new ComputerDto("komputer 1", LocalDate.parse("2020-02-05"),
                        new BigDecimal("0"),new BigDecimal("1426.24")), 0),
                Arguments.of(new ComputerDto("komputer 1", LocalDate.parse("2020-02-05"),
                        null,new BigDecimal("1426.24")), 1)
        );
    }

    @BeforeEach
    public void createValidator() {
        validatorFactory = Validation.buildDefaultValidatorFactory();
        validator = validatorFactory.getValidator();
    }

    @AfterEach
    public void close() {
        validatorFactory.close();
    }

    @DisplayName("Should validate")
    @ParameterizedTest(name = "Should return {1} violations for {0}")
    @MethodSource("getComputers")
    void shouldValidate(final ComputerDto invalidComputer, final int violationsNumber) {
        //given
        //when
        final Set<ConstraintViolation<ComputerDto>> violations = validator.validate(invalidComputer);
        //then
        assertThat(violations).hasSize(violationsNumber);
    }
}