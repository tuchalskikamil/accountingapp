package pl.tuchalski.kamil.accountingApp.computer;

import org.json.JSONException;
import org.junit.jupiter.api.*;
import org.mockserver.client.MockServerClient;
import org.mockserver.integration.ClientAndServer;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.ActiveProfiles;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockserver.integration.ClientAndServer.startClientAndServer;
import static org.mockserver.model.HttpRequest.request;
import static org.mockserver.model.HttpResponse.response;
import static org.mockserver.model.MediaType.APPLICATION_JSON;
import static org.mockserver.model.Parameter.param;
import static pl.tuchalski.kamil.accountingApp.reader.FileReader.*;


@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class ComputerE2ETest {

    private static ClientAndServer mockServer;
    @Autowired
    private TestRestTemplate restTemplate;
    @Autowired
    private ComputerRepository computerRepository;
    @LocalServerPort
    private int servicePort;

    @BeforeAll
    public static void setupMock() {
        mockServer = startClientAndServer(1080);
    }

    @AfterAll
    public static void closeMock() {
        mockServer.stop();
    }

    @BeforeEach
    public void startMockServer() throws IOException {
        new MockServerClient("localhost", 1080)
                .when(
                        request()
                                .withMethod("GET")
                                .withPath("/exchangerates/rates/a/usd/2022-01-05/")
                                .withQueryStringParameters(param("format", "json"))
                )
                .respond(response().withBody(readTestFile("computer/mockApi.json")).withContentType(APPLICATION_JSON));
    }

    @Test
    @DisplayName("Should return status 200 - OK")
    void shouldReturnStatusOk() throws IOException, JSONException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readTestFile("computer/computer.json"), headers);
        //when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/computer",
                request,
                String.class
        );
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        JSONAssert.assertEquals(
                readTestFile("computer/computerReturn.json"),
                result.getBody(),
                JSONCompareMode.NON_EXTENSIBLE
        );
    }

    @Test
    @DisplayName("Should return filtered and sorted computers")
    void shouldReturnFilteredAndSortedComputers() throws IOException, JSONException {
        // given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> requestSave = new HttpEntity<>(readTestFile("computer/computerToSortAndFilter.json"),
                headers);
        restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/computer",
                requestSave,
                String.class
        );
        // when
        final ResponseEntity<String> result = restTemplate.getForEntity(
                "http://localhost:" + servicePort + "/computer?name=aaa&sortedBy=name",
                String.class
        );
        // then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.OK);
        JSONAssert.assertEquals(
                readTestFile("computer/computerReturnSortedAndFiltered.json"),
                result.getBody(),
                JSONCompareMode.NON_EXTENSIBLE
        );
    }

    @Test
    @DisplayName("Should return status bad request")
    void shouldReturnStatusBadRequest() throws IOException, JSONException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readTestFile("computer/computerBadRequest.json"), headers);
        //when
        final ResponseEntity<String> result = restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/computer",
                request,
                String.class
        );
        //then
        assertThat(result.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        JSONAssert.assertEquals(
                readTestFile("error/validationError.json"),
                result.getBody(),
                JSONCompareMode.NON_EXTENSIBLE
        );
    }

    @Test
    @DisplayName("Should save computer")
    void shouldSaveComputer() throws IOException {
        //given
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        final HttpEntity<String> request = new HttpEntity<>(readTestFile("computer/computer.json"), headers);
        //when
        restTemplate.postForEntity(
                "http://localhost:" + servicePort + "/computer",
                request,
                String.class
        );
        //then
        final Computer computer = computerRepository.findAll().get(0);
        assertThat(computer.getName()).isEqualTo("komputer 1");
        assertThat(computer.getPostingDate()).isEqualTo(LocalDate.parse("2022-01-05"));
        assertThat(computer.getCostUsd()).isEqualTo(new BigDecimal("123.00"));
        assertThat(readInvoice()).isEqualTo(readTestFile("computer/invoices.xml"));
    }

    @AfterEach
    void cleanup() throws IOException {
        computerRepository.deleteAll();
        removeInvoices();
    }
}
