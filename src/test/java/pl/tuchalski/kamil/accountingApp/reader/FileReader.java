package pl.tuchalski.kamil.accountingApp.reader;

import org.apache.commons.io.FileUtils;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Class reading from file
 */
public class FileReader {

    public static String readTestFile(final String path) throws IOException {
        return FileUtils.readFileToString(
                ResourceUtils.getFile("src/test/resources/" + path),
                StandardCharsets.UTF_8
        );
    }

    public static String readInvoice() throws IOException {
        final File invoiceFile = ResourceUtils.getFile("src/test/resources/invoices").listFiles()[0];
        return FileUtils.readFileToString(
                invoiceFile,
                StandardCharsets.UTF_8
        );
    }

    public static void removeInvoices() throws IOException {
        final File[] invoiceFiles = ResourceUtils.getFile("src/test/resources/invoices").listFiles();
        for (final File file : invoiceFiles) {
            file.delete();
        }
    }
}
